package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.dto.IDtoUserService;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.ExistLoginException;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.repository.dto.TaskDtoRepository;
import ru.mtumanov.tm.repository.dto.UserDtoRepository;
import ru.mtumanov.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class UserDtoService extends AbstractDtoService<UserDTO, IDtoUserRepository> implements IDtoUserService {

    @NotNull
    protected final IPropertyService propertyService;

    @NotNull
    protected IDtoTaskRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @NotNull
    protected IDtoProjectRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    public UserDtoService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    protected IDtoUserRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDtoRepository(entityManager);
    }

    @Override
    @NotNull
    public UserDTO create(@NotNull final String login, @NotNull final String password) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    @NotNull
    public UserDTO create(@NotNull final String login, @NotNull final String password, @NotNull final String email) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();
        if (email.isEmpty())
            throw new EmailEmptyException();

        @NotNull final UserDTO user = create(login, password);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        user.setEmail(email);
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;

    }

    @Override
    @NotNull
    public UserDTO create(@NotNull final String login, @NotNull final String password, @NotNull final Role role) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();
        if (isLoginExist(login))
            throw new ExistLoginException();
        if (password.isEmpty())
            throw new PasswordEmptyException();


        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final UserDTO user = create(login, password);
        user.setRole(role);
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;

    }

    @Override
    @NotNull
    public UserDTO findByLogin(@NotNull final String login) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (login.isEmpty())
            throw new LoginEmptyException();
        return getRepository(entityManager).findByLogin(login);
    }

    @Override
    @NotNull
    public UserDTO findByEmail(@NotNull final String email) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (email.isEmpty())
            throw new EmailEmptyException();
        return getRepository(entityManager).findByEmail(email);

    }

    @Override
    @NotNull
    public UserDTO findById(@NotNull final String id) throws AbstractException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (id.isEmpty())
            throw new EmailEmptyException();
        return getRepository(entityManager).findOneById(id);
    }

    @Override
    @NotNull
    public UserDTO removeByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final UserDTO user = getRepository(entityManager).findByLogin(login);
        try {
            entityManager.getTransaction().begin();
            getTaskRepository(entityManager).clear(user.getId());
            getProjectRepository(entityManager).clear(user.getId());
            getRepository(entityManager).removeById(user.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    @NotNull
    public UserDTO removeByEmail(@NotNull final String email) throws AbstractException {
        if (email.isEmpty())
            throw new EmailEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final UserDTO user = getRepository(entityManager).findByEmail(email);
        try {
            entityManager.getTransaction().begin();
            getTaskRepository(entityManager).clear(user.getId());
            getProjectRepository(entityManager).clear(user.getId());
            getRepository(entityManager).removeById(user.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    @NotNull
    public UserDTO setPassword(@NotNull final String id, @NotNull final String password) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (password.isEmpty())
            throw new PasswordEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final UserDTO user = getRepository(entityManager).findOneById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    @NotNull
    public UserDTO userUpdate(
            @NotNull final String id,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final UserDTO user = getRepository(entityManager).findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (login.isEmpty())
            return false;
        return getRepository(entityManager).isLoginExist(login);

    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        if (email.isEmpty())
            return false;
        return getRepository(entityManager).isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final UserDTO user = getRepository(entityManager).findByLogin(login);
        user.setLocked(true);
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@NotNull final String login) throws AbstractException {
        if (login.isEmpty())
            throw new LoginEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final UserDTO user = getRepository(entityManager).findByLogin(login);
        user.setLocked(false);
        try {
            entityManager.getTransaction().begin();
            getRepository(entityManager).update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
