package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.endpoint.ITaskEndpoint;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.dto.model.SessionDTO;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.request.task.*;
import ru.mtumanov.tm.dto.response.task.*;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.enumerated.TaskSort;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Comparator;
import java.util.List;

@WebService(endpointInterface = "ru.mtumanov.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskChangeStatusByIdRs taskChangeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final Status status = request.getStatus();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().changeTaskStatusById(userId, id, status);
            return new TaskChangeStatusByIdRs(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskChangeStatusByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskClearRs taskClear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            getServiceLocator().getTaskService().clear(userId);
            return new TaskClearRs();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskClearRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskCompleteByIdRs taskCompleteById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
            return new TaskCompleteByIdRs(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCompleteByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskCreateRs taskCreate(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().create(userId, name, description);
            return new TaskCreateRs(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskCreateRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskListRs taskList(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            ;
            @Nullable final String userId = session.getUserId();
            @Nullable final TaskSort sort = request.getSort();
            Comparator<TaskDTO> comparator = null;
            if (sort != null)
                comparator = sort.getComparator();
            @NotNull final List<TaskDTO> tasks = getServiceLocator().getTaskService().findAll(userId, comparator);
            return new TaskListRs(tasks);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskListRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskRemoveByIdRs taskRemoveById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            getServiceLocator().getTaskService().removeById(userId, id);
            return new TaskRemoveByIdRs();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskRemoveByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskShowByIdRs taskShowById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().findOneById(userId, id);
            return new TaskShowByIdRs(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskStartByIdRs taskStartById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
            return new TaskStartByIdRs(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskStartByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUpdateByIdRs taskUpdateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String id = request.getId();
            @Nullable final String name = request.getName();
            @Nullable final String description = request.getDescription();
            @NotNull final TaskDTO task = getServiceLocator().getTaskService().updateById(userId, id, name, description);
            return new TaskUpdateByIdRs(task);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUpdateByIdRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskBindToProjectRs taskBindToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
            return new TaskBindToProjectRs();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskBindToProjectRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUnbindFromProjectRs taskUnbindFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @Nullable final String taskId = request.getTaskId();
            getServiceLocator().getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
            return new TaskUnbindFromProjectRs();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskUnbindFromProjectRs(e);
        }
    }

    @Override
    @NotNull
    @WebMethod
    public TaskShowByProjectIdRs taskShowByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByProjectIdRq request
    ) {
        try {
            @NotNull final SessionDTO session = check(request);
            @Nullable final String userId = session.getUserId();
            @Nullable final String projectId = request.getProjectId();
            @NotNull final List<TaskDTO> tasks = getServiceLocator().getTaskService().findAllByProjectId(userId, projectId);
            return new TaskShowByProjectIdRs(tasks);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
            return new TaskShowByProjectIdRs(e);
        }
    }

}
