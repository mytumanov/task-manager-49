package ru.mtumanov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.service.dto.IDtoProjectService;
import ru.mtumanov.tm.api.service.dto.IDtoProjectTaskService;
import ru.mtumanov.tm.api.service.dto.IDtoTaskService;
import ru.mtumanov.tm.api.service.dto.IDtoUserService;

public interface IServiceLocator {

    @NotNull
    IDtoProjectService getProjectService();

    @NotNull
    IDtoTaskService getTaskService();

    @NotNull
    IDtoProjectTaskService getProjectTaskService();

    @NotNull
    IDtoUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ILoggerService getLoggerService();

}
