package ru.mtumanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.model.TaskDTO;

import java.util.List;

public interface IDtoTaskRepository extends IDtoUserOwnedRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTaskByProjectId(@NotNull String projectId);

}
