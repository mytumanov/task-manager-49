package ru.mtumanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    User findByEmail(@NotNull String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}
