package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.model.IUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Comparator;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void clear() {
        @NotNull final List<User> users = findAll();
        for (@NotNull final User user : users) {
            remove(user);
        }
    }

    @Override
    @NotNull
    public List<User> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return entityManager.createQuery("FROM User p", User.class).getResultList();
        } finally {
            entityManager.close();
        }
    }


    @Override
    @NotNull
    public List<User> findAll(@NotNull final Comparator<User> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return entityManager.createQuery("FROM User p ORDER BY p." + getComporator(comparator), User.class).getResultList();
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public User findOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return entityManager.find(User.class, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return entityManager.createQuery("SELECT COUNT(1) FROM User p", Long.class)
                    .getSingleResult();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final User project = findOneById(id);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public @NotNull User findByLogin(@NotNull final String login) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return entityManager.createQuery("FROM User p WHERE login = :login", User.class)
                    .setParameter("login", login)
                    .getSingleResult();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @NotNull User findByEmail(@NotNull final String email) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            return entityManager.createQuery("FROM User p WHERE email = :email", User.class)
                    .setParameter("email", email)
                    .getSingleResult();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        try {
            findByLogin(login);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        try {
            findByEmail(email);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

}
