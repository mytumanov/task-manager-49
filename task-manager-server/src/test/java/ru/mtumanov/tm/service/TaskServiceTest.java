package ru.mtumanov.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.api.service.dto.IDtoTaskService;
import ru.mtumanov.tm.api.service.dto.IDtoUserService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.migration.AbstractSchemeTest;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.repository.dto.TaskDtoRepository;
import ru.mtumanov.tm.service.dto.TaskDtoService;
import ru.mtumanov.tm.service.dto.UserDtoService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@Category(DBCategory.class)
public class TaskServiceTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static UserDTO USER1;

    @NotNull
    private static final ProjectDTO projectUser1 = new ProjectDTO();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IDtoTaskService taskService = new TaskDtoService(connectionService);

    @NotNull
    private static final IDtoUserService userService = new UserDtoService(connectionService, propertyService);

    @NotNull
    private List<TaskDTO> taskList = new ArrayList<>();

    @BeforeClass
    public static void initData() throws AbstractException, LiquibaseException {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");

        liquibase.dropAll();
        liquibase.update("scheme");

        USER1 = userService.create("UserFirstName 10", "password", Role.USUAL);
        projectUser1.setName("Test task name");
        projectUser1.setDescription("Test task description");
        projectUser1.setUserId(USER1.getId());
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(projectUser1);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Before
    public void initRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);

        try {
            entityManager.getTransaction().begin();
            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull final TaskDTO task = new TaskDTO();
                task.setName("TaskDTO name: " + i);
                task.setDescription("TaskDTO description: " + i);
                task.setUserId(USER1.getId());
                task.setProjectId(projectUser1.getId());
                taskRepository.add(task);
                taskList.add(task);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void clearRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        taskList.clear();
    }

    @Test
    public void testChangeTaskStatusById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            int i = 0;
            for (@NotNull final TaskDTO task : taskList) {
                if (i % 2 == 0) {
                    taskService.changeTaskStatusById(task.getUserId(), task.getId(), Status.IN_PROGRESS);
                    entityManager.clear();
                    @NotNull final TaskDTO actualTask = taskRepository.findOneById(task.getUserId(), task.getId());
                    assertEquals(Status.IN_PROGRESS, actualTask.getStatus());
                }
                if (i % 3 == 0) {
                    taskService.changeTaskStatusById(task.getUserId(), task.getId(), Status.COMPLETED);
                    entityManager.clear();
                    @NotNull final TaskDTO actualTask = taskRepository.findOneById(task.getUserId(), task.getId());
                    assertEquals(Status.COMPLETED, actualTask.getStatus());
                }
                i++;
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test(expected = IdEmptyException.class)
    public void testExceptionChangeTaskStatusById() throws Exception {
        taskService.changeTaskStatusById(UUID.randomUUID().toString(), "", Status.IN_PROGRESS);
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final String name = "TEST project";
            @NotNull final String description = "DEscription";
            @NotNull final TaskDTO task = taskService.create(USER1.getId(), name, description);
            @NotNull final TaskDTO actualTask = taskRepository.findOneById(task.getUserId(), task.getId());
            assertEquals(name, actualTask.getName());
            assertEquals(description, actualTask.getDescription());
            assertEquals(USER1.getId(), actualTask.getUserId());
            assertEquals(taskRepository.getSize(task.getUserId()), taskList.size() + 1);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test(expected = NameEmptyException.class)
    public void testExceptionCreate() throws Exception {
        @NotNull final String name = "";
        @NotNull final String description = "DEscription";
        taskService.create(USER1.getId(), name, description);
    }

    @Test
    public void testFindAllByProjectId() throws Exception {
        assertEquals(taskList, taskService.findAllByProjectId(USER1.getId(), projectUser1.getId()));
    }

    @Test
    public void testUpdateById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoTaskRepository taskRepository = new TaskDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            for (@NotNull final TaskDTO task : taskList) {
                @NotNull final String name = task.getName() + "TEST";
                @NotNull final String description = task.getDescription() + "TEST";
                taskService.updateById(task.getUserId(), task.getId(), name, description);
                @NotNull final TaskDTO actualTask = taskRepository.findOneById(task.getUserId(), task.getId());
                assertEquals(name, actualTask.getName());
                assertEquals(description, actualTask.getDescription());
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
