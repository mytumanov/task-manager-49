package ru.mtumanov.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoUserRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.dto.model.UserDTO;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.marker.DBCategory;
import ru.mtumanov.tm.migration.AbstractSchemeTest;
import ru.mtumanov.tm.repository.dto.ProjectDtoRepository;
import ru.mtumanov.tm.repository.dto.UserDtoRepository;
import ru.mtumanov.tm.service.ConnectionService;
import ru.mtumanov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@Category(DBCategory.class)
public class ProjectRepositoryTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private UserDTO USER1;

    @NotNull
    private final List<ProjectDTO> projectList = new ArrayList<>();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @BeforeClass
    public static void init() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = new UserDTO();
            user.setFirstName("UserFirstName 1");
            user.setLastName("UserLastName 1");
            user.setMiddleName("UserMidName 1");
            user.setEmail("user1@dot.ru");
            user.setLogin("USER1");
            user.setRole(Role.USUAL);
            user.setPasswordHash("123");
            userRepository.add(user);
            entityManager.flush();
            USER1 = user;
            for (int j = 0; j < NUMBER_OF_ENTRIES; j++) {
                @NotNull final ProjectDTO ProjectDTO = new ProjectDTO();
                ProjectDTO.setName("ProjectDTO name: " + j);
                ProjectDTO.setDescription("ProjectDTO description: " + j);
                ProjectDTO.setUserId(user.getId());
                ProjectDtoRepository.add(ProjectDTO);
                projectList.add(ProjectDTO);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void clearRepository() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
        @NotNull final IDtoUserRepository userRepository = new UserDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            ProjectDtoRepository.clear();
            userRepository.clear();
            projectList.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final ProjectDTO ProjectDTO = new ProjectDTO();
        ProjectDTO.setName("Test ProjectDTO name");
        ProjectDTO.setDescription("Test ProjectDTO description");
        ProjectDTO.setUserId(USER1.getId());

        projectList.add(ProjectDTO);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            ProjectDtoRepository.add(ProjectDTO);
            entityManager.getTransaction().commit();
            @NotNull final List<ProjectDTO> actualProjectList = ProjectDtoRepository.findAll();
            assertEquals(projectList.size(), actualProjectList.size());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testClear() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
        assertNotEquals(0, ProjectDtoRepository.getSize(USER1.getId()));
        try {
            entityManager.getTransaction().begin();
            ProjectDtoRepository.clear();
            entityManager.getTransaction().commit();
            assertEquals(0, ProjectDtoRepository.getSize(USER1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testExistById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
        for (@NotNull final ProjectDTO ProjectDTO : projectList) {
            assertTrue(ProjectDtoRepository.existById(USER1.getId(), ProjectDTO.getId()));
        }
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
        @NotNull final List<ProjectDTO> actualProjectsUser1 = ProjectDtoRepository.findAll(USER1.getId());
        assertEquals(projectList, actualProjectsUser1);
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
        for (@NotNull final ProjectDTO ProjectDTO : projectList) {
            assertEquals(ProjectDTO, ProjectDtoRepository.findOneById(USER1.getId(), ProjectDTO.getId()));
        }
    }

    @Test(expected = NoResultException.class)
    public void testExceptionFindOneById() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
        for (int i = 0; i < 10; i++) {
            ProjectDtoRepository.findOneById(USER1.getId(), UUID.randomUUID().toString());
        }
    }

    @Test
    public void testGetSize() throws Exception {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
        assertEquals(projectList.size(), ProjectDtoRepository.getSize(USER1.getId()));
    }

    @Test
    public void testRemove() throws Exception {
        for (@NotNull final ProjectDTO ProjectDTO : projectList) {
            @NotNull final EntityManager entityManager = connectionService.getEntityManager();
            @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
            assertTrue(ProjectDtoRepository.existById(USER1.getId(), ProjectDTO.getId()));
            try {
                entityManager.getTransaction().begin();
                ProjectDtoRepository.removeById(USER1.getId(), ProjectDTO.getId());
                entityManager.getTransaction().commit();
                assertFalse(ProjectDtoRepository.existById(USER1.getId(), ProjectDTO.getId()));
            } catch (@NotNull final Exception e) {
                entityManager.getTransaction().rollback();
                throw e;
            } finally {
                entityManager.close();
            }
        }
    }

    @Test
    public void testRemoveById() throws Exception {
        for (@NotNull final ProjectDTO ProjectDTO : projectList) {
            @NotNull final EntityManager entityManager = connectionService.getEntityManager();
            @NotNull final IDtoProjectRepository ProjectDtoRepository = new ProjectDtoRepository(entityManager);
            assertTrue(ProjectDtoRepository.existById(USER1.getId(), ProjectDTO.getId()));
            try {
                entityManager.getTransaction().begin();
                ProjectDtoRepository.removeById(ProjectDTO.getId());
                entityManager.getTransaction().commit();
                assertFalse(ProjectDtoRepository.existById(USER1.getId(), ProjectDTO.getId()));
            } catch (@NotNull final Exception e) {
                entityManager.getTransaction().rollback();
                throw e;
            } finally {
                entityManager.close();
            }
        }
    }

}
