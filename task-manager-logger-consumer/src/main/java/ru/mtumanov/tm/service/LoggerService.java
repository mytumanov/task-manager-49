package ru.mtumanov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new YAMLMapper();

    public void log(@NotNull final String yaml) throws IOException {
        @NotNull final Map<String, Object> event = objectMapper.readValue(yaml, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        @Nullable final byte[] bytes = yaml.getBytes();
        @NotNull final File file = new File(table);
        if (!file.exists())
            file.createNewFile();
        Files.write(Paths.get(table), bytes, StandardOpenOption.APPEND);
    }

}
