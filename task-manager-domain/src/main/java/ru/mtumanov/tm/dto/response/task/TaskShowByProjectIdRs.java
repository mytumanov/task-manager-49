package ru.mtumanov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.response.AbstractResultRs;

import java.util.List;

@NoArgsConstructor
public class TaskShowByProjectIdRs extends AbstractResultRs {

    @Nullable
    @Getter
    @Setter
    private List<TaskDTO> tasks;

    public TaskShowByProjectIdRs(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

    public TaskShowByProjectIdRs(@NotNull final Throwable err) {
        super(err);
    }

}
