package ru.mtumanov.tm.dto.response.server;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.response.AbstractRs;

@Getter
@Setter
public class ServerAboutRs extends AbstractRs {

    @Nullable
    private String email;

    @Nullable
    private String name;

}
