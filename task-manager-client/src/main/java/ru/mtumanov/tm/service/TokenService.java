package ru.mtumanov.tm.service;

import lombok.Getter;
import lombok.Setter;
import ru.mtumanov.tm.api.service.ITokenService;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TokenService implements ITokenService {

    @NotNull
    private String token;

}
